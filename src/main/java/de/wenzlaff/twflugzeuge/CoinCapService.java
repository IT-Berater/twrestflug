package de.wenzlaff.twflugzeuge;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;

/**
 * 
 * https://docs.coincap.io/#f8869879-171f-4240-adfd-dd2947506adc
 * 
 * 
 * curl -X 'GET' 'https://api.coincap.io/v2/assets/bitcoin' -H 'accept:
 * text/plain'
 * 
 * 
 * https://github.com/public-apis/public-apis#cryptocurrency
 */

@RegisterRestClient(configKey = "CoinCapService")
public interface CoinCapService {

	// http://localhost:8080/coin/btc
	// search by asset id (bitcoin) or symbol (BTC)

	@Path("/assets")
	@GET
	String getById(@QueryParam(value = "search") String search);

	@Path("/rates")
	@GET
	String getRates(@QueryParam(value = "search") String search);
}
