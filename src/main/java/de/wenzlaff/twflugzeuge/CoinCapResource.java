package de.wenzlaff.twflugzeuge;

import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("v1")
public class CoinCapResource {

	@Inject
	Logger log;

	@Inject
	@RestClient
	CoinCapService coinCapService;

	@GET
	@Path("coin/{search}")
	@Counted(name = "anzahlCoinSuchen", description = "Anzahl der Coin suchen.")
	public String id(@PathParam(value = "search") String search) {

		String erg = coinCapService.getById(search);

		log.info(erg);

		return erg;
	}

	@GET
	@Path("rates/{search}")
	@Counted(name = "anzahlRatesSuchen", description = "Anzahl der Rates suchen.")
	public String getRates(@PathParam(value = "search") String search) {

		String erg = coinCapService.getRates(search);

		log.info(erg);

		return erg;
	}

}
