package de.wenzlaff.twflugzeuge;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;

import io.quarkus.hibernate.orm.rest.data.panache.PanacheEntityResource;
import io.quarkus.rest.data.panache.ResourceProperties;

/**
 * https://quarkus.io/guides/rest-data-panache
 */
@OpenAPIDefinition(info = @Info(title = "Flugzeug API", version = "1.0.0", description = "Flugzeug Client", contact = @Contact(url = "http://www.wenzlaff.info", name = "Thomas Wenzlaff", email = "info@wenzlaff.info")))
@ResourceProperties(path = "v1/flugzeug")
public interface FlugzeugResource extends PanacheEntityResource<Flugzeug, Long> {

}